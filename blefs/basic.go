package blefs

import (
	"io/fs"
	"path/filepath"
)

// Stat does a ReadDir() and finds the current file in the output
func (blefs *FS) Stat(path string) (fs.FileInfo, error) {
	// Get directory in filepath
	dir := filepath.Dir(path)
	// Read directory
	dirEntries, err := blefs.ReadDir(dir)
	if err != nil {
		return nil, err
	}
	for _, entry := range dirEntries {
		// If file name is base name of path
		if entry.Name() == filepath.Base(path) {
			// Return file info
			return entry.Info()
		}
	}
	return nil, ErrFileNotExists
}

// Rename moves or renames a file or directory
func (blefs *FS) Rename(old, new string) error {
	// Create move request
	err := blefs.request(
		FSCmdMove,
		true,
		uint16(len(old)),
		uint16(len(new)),
		old,
		byte(0x00),
		new,
	)
	if err != nil {
		return err
	}
	var status int8
	// Upon receiving 0x61 (FSResponseMove)
	blefs.on(FSResponseMove, func(data []byte) error {
		// Read status byte
		return decode(data, &status)
	})
	// If status is not ok, return error
	if status != FSStatusOk {
		return FSError{status}
	}
	return nil
}

// Remove removes a file or directory
func (blefs *FS) Remove(path string) error {
	// Create delete request
	err := blefs.request(
		FSCmdDelete,
		true,
		uint16(len(path)),
		path,
	)
	if err != nil {
		return err
	}
	var status int8
	// Upon receiving 0x31 (FSResponseDelete)
	blefs.on(FSResponseDelete, func(data []byte) error {
		// Read status byte
		return decode(data, &status)
	})
	if status != FSStatusOk {
		// If status is not ok, return error
		return FSError{status}
	}
	return nil
}
